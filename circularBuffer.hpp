#ifndef F_CIRCULARBUFFER_HPP_INCLUDED
#define F_CIRCULARBUFFER_HPP_INCLUDED

#include <memory>
#include <vector>

template <typename ELEM_TYPE, size_t NUM_ELEMS>
class CircularBuffer
{
    std::unique_ptr<ELEM_TYPE[]> m_data;
    size_t m_size, m_begin, m_end;

public:
    explicit CircularBuffer();
    CircularBuffer(const CircularBuffer<ELEM_TYPE, NUM_ELEMS> &buf) = delete;
    CircularBuffer(const CircularBuffer<ELEM_TYPE, NUM_ELEMS> &&buf) = delete;
    CircularBuffer<ELEM_TYPE, NUM_ELEMS>& operator=(const CircularBuffer<ELEM_TYPE, NUM_ELEMS> &buf) = delete;
    CircularBuffer<ELEM_TYPE, NUM_ELEMS>& operator=(const CircularBuffer<ELEM_TYPE, NUM_ELEMS> &&buf) = delete;

    void add(ELEM_TYPE elem);
    bool empty() const {return m_size == 0;}
    size_t size() const {return m_size;}
    void clear();

    std::vector<ELEM_TYPE> toVector() const;

    class Iterator
    {
        const CircularBuffer<ELEM_TYPE, NUM_ELEMS> *m_buffer;
        size_t m_index;

        public:
        Iterator(const CircularBuffer<ELEM_TYPE, NUM_ELEMS> *buffer, size_t index) : m_buffer(buffer), m_index(index) {}

        Iterator& operator++();
        Iterator operator++(int);
        bool operator==(const Iterator &other) const;
        bool operator!=(const Iterator &other) const;

        const ELEM_TYPE& operator*() const;
        const ELEM_TYPE* operator->() const;

        friend class CircularBuffer<ELEM_TYPE, NUM_ELEMS>;
    };

    Iterator begin() const;
    Iterator end() const;

    const ELEM_TYPE& operator[](size_t index) const;
};

template <typename ELEM_TYPE, size_t NUM_ELEMS>
CircularBuffer<ELEM_TYPE, NUM_ELEMS>::CircularBuffer() :
    m_data(new ELEM_TYPE[NUM_ELEMS]),
    m_size(0),
    m_begin(0),
    m_end(0)
{
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
typename CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator CircularBuffer<ELEM_TYPE, NUM_ELEMS>::begin() const
{
    return Iterator(this, 0);
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
typename CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator CircularBuffer<ELEM_TYPE, NUM_ELEMS>::end() const
{
    Iterator it(this, size());
    return it;
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
typename CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator& CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::operator++()
{
    ++m_index;
    return *this;
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
typename CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::operator++(int)
{
    Iterator temp(*this);
    operator++();
    return temp;
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
bool CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::operator==(const typename CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::Iterator &other) const
{
    return m_index == other.m_index;
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
bool CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::operator!=(const typename CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::Iterator &other) const
{
    return !operator==(other);
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
const ELEM_TYPE& CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::operator*() const
{
    return m_buffer->operator[](m_index);
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
const ELEM_TYPE* CircularBuffer<ELEM_TYPE, NUM_ELEMS>::Iterator::operator->() const
{
    return &operator*();
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
const ELEM_TYPE& CircularBuffer<ELEM_TYPE, NUM_ELEMS>::operator[](size_t index) const
{
    return m_data[(m_begin + index) % NUM_ELEMS];
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
void CircularBuffer<ELEM_TYPE, NUM_ELEMS>::add(ELEM_TYPE elem)
{
    if (empty())
    {
        m_data[m_end] = elem;
        ++m_size;
        return;
    }

    m_end = (m_end + 1) % NUM_ELEMS;
    m_data[m_end] = elem;

    if (m_end == m_begin)
    {
        m_begin = (m_begin + 1) % NUM_ELEMS;
        m_size = NUM_ELEMS;
    }
    else
    {
        ++m_size;
    }
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
void CircularBuffer<ELEM_TYPE, NUM_ELEMS>::clear()
{
    m_size = 0;
    m_begin = 0;
    m_end = 0;
}

template <typename ELEM_TYPE, size_t NUM_ELEMS>
std::vector<ELEM_TYPE> CircularBuffer<ELEM_TYPE, NUM_ELEMS>::toVector() const
{
    std::vector<ELEM_TYPE> vec;
    vec.reserve(size());

    for (const auto &elem : *this)
    {
        vec.push_back(elem);
    }

    return vec;
}

#endif
