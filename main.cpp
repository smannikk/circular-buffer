#include <iostream>

#include "circularBuffer.hpp"

int main()
{
    CircularBuffer<int, 64> test;

    for (int i=0; i<128; ++i)
    {
        test.add(i);
    }

    for (int i=0; (size_t)i<test.size(); ++i)
    {
        if (test[i] != i+64)
        {
            std::cout << "Test failed.\n";
            return 1;
        }
    }
    std::cout << "Test passed.\n";

    return 0;
}
